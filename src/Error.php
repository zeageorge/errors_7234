<?php

declare(strict_types=1);

namespace zeageorge\errors_7234;

use JsonSerializable;
use Throwable;

/**
 * Description of Error
 *
 * @author George Zeakis <zeageorge@gmail.com>
 */
class Error implements JsonSerializable {
  protected $code = '';
  protected $message = '';
  protected $display_message = '';
  protected $description = '';
  protected $exception = null;
  protected $previous = null;
  protected $data = null;

  /**
   * Constructor
   *
   * @param string $code
   * @param string $message
   * @param string $display_message
   * @param string $description
   * @param Throwable|null $exception
   * @param self|null $previous
   * @param mixed $data
   */
  public function __construct(array $params = []) {
    foreach ($params + [
      'code' => '',
      'message' => '',
      'display_message' => '',
      'description' => '',
      'exception' => null,
      'previous' => null,
      'data' => null
    ] as $key => $value) {
      $this->$key = $value;
    }
  }

  /**
   *
   * @return string|null
   */
  public function getCode(): ?string {
    return $this->code;
  }

  /**
   *
   * @param string $code
   * @return self
   */
  public function setCode(string $code): self {
    $this->code = $code;

    return $this;
  }

  /**
   *
   * @return string|null
   */
  public function getMessage(): ?string {
    return $this->message;
  }

  /**
   *
   * @param string $message
   * @return self
   */
  public function setMessage(string $message): self {
    $this->message = $message;

    return $this;
  }

  /**
   *
   * @return string|null
   */
  public function getDisplayMessage(): ?string {
    return $this->display_message;
  }

  /**
   *
   * @param string $display_message
   * @return self
   */
  public function setDisplayMessage(string $display_message): self {
    $this->display_message = $display_message;

    return $this;
  }

  /**
   *
   * @return string|null
   */
  public function getDescription(): ?string {
    return $this->description;
  }

  /**
   *
   * @param string $description
   * @return self
   */
  public function setDescription(string $description): self {
    $this->description = $description;

    return $this;
  }

  /**
   *
   * @return Throwable|null
   */
  public function getException(): ?Throwable {
    return $this->exception;
  }

  /**
   *
   * @param Throwable $exception
   * @return self
   */
  public function setException(Throwable $exception): self {
    $this->exception = $exception;

    return $this;
  }

  /**
   *
   * @return self|null
   */
  public function getPrevious(): ?self {
    return $this->previous;
  }

  /**
   *
   * @param self $previous
   * @return self
   */
  public function setPrevious(self $previous): self {
    $this->previous = $previous;

    return $this;
  }

  /**
   *
   * @return mixed
   */
  public function getData() {
    return $this->data;
  }

  /**
   *
   * @param mixed $data
   * @return self
   */
  public function setData($data): self {
    $this->data = $data;

    return $this;
  }

  /**
   *
   * @return mixed
   */
  public function jsonSerialize() {
    return (object) [
      'code' => $this->code,
      'message' => $this->message,
      'display_message' => $this->display_message,
      'description' => $this->description,
      'data' => $this->data,
      'exception' => $this->exception,
      'previous' => $this->previous,
    ];
  }
}
