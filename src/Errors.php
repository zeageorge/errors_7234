<?php

declare(strict_types=1);

namespace zeageorge\errors_7234;

use JsonSerializable, Countable;
use function count, array_merge;

/**
 * Description of Errors
 *
 * @author George Zeakis <zeageorge@gmail.com>
 */
class Errors implements JsonSerializable, Countable {
  /** @var Error[] */
  protected $errors;

  /**
   * Constructor
   *
   * @param Error ...$errors
   */
  public function __construct(Error ...$errors) {
    $this->errors = $errors ?? [];
  }

  /**
   *
   * @return Error[]
   */
  public function toArray(): array {
    return $this->errors;
  }

  /**
   *
   * @param Error ...$errors
   * @return self
   */
  public function add(Error ...$errors): self {
    return $this->merge(new Errors(...$errors));
  }

  /**
   *
   * @return self
   */
  public function clear(): self {
    $this->errors = [];

    return $this;
  }

  /**
   *
   * @return int
   */
  public function count(): int {
    return count($this->errors);
  }

  /**
   *
   * @param Errors $errors
   * @return self
   */
  public function merge(Errors $errors): self {
    $this->errors = array_merge($this->errors, $errors->toArray());

    return $this;
  }

  /**
   *
   * @return Error[]
   */
  public function jsonSerialize() {
    return $this->errors;
  }
}
