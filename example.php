<?php

require 'vendor/autoload.php';

use zeageorge\errors_7234\{Error, Errors};
use function json_encode;
use const JSON_UNESCAPED_UNICODE, JSON_UNESCAPED_SLASHES, JSON_THROW_ON_ERROR;

$error = new Error(['code' => 'SOME_UNIQUE_CODE', 'message' => 'Something is wrong']);

$errors = new Errors($error);

echo json_encode($errors, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_THROW_ON_ERROR);
